#!/usr/bin/env/ python3
import click
import sys
from subprocess import run
from ruamel.yaml import YAML
from pathlib import Path
from whichcraft import which

local_fpath = Path(__file__)
local_dir = local_fpath.parent


def load_config_file(config=None):
    y = YAML(typ="safe")
    config = config if config else local_dir.joinpath("config.yml")
    config_dict = y.load(config)
    return config_dict


def set_env(config_data):
    vals = []
    for k, v in config_data.items():
        if isinstance(v, str) and v[0] == "$":
            vals.append(f"export {k}={v}")
            print(v)
        else:
            vals.append(f'export {k}="{v}"')
    envrc = Path.cwd().joinpath(".envrc")
    envrc.touch()
    with open(envrc, "w") as file:
        for val in vals:
            print(val, file=file)
    run("direnv allow", shell=True)


@click.command()
@click.option(
    "-e",
    "--env",
    required=True,
    help="""Path to YAML file that contains a map of environment variable values.""",
)
def main(env):
    try:
        assert which("direnv")
    except AssertionError as e:
        print(
            "Direnv is not installed. Installation information can be found at https://direnv.net"
        )
        sys.exit(1)
    try:
        env = Path(env)
        config_file = load_config_file(config=env)
        set_env(config_file)
    except FileNotFoundError:
        print(f'Environment file "{env}" not found. Please verify path')
        sys.exit(1)


if __name__ == "__main__":
    main()
