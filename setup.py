from setuptools import setup

setup(
    name="swenv",
    version="1.0",
    install_requires=["Click", "ruamel.yaml", "whichcraft"],
    py_modules=["swenv"],
    entry_points="""
    [console_scripts]
    swenv=swenv:main
""",
)
